package com.auvn.browser.data.propertiesfile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.auvn.browser.objects.PageObject;

public class Properties {

	private static BufferedReader propertiesFileReader;
	private static Matcher matcher;
	private static String tempStr;

	public static void setProperties(File propertiesFile,
			PageObject pageObject, Pattern pattern) throws IOException {

		propertiesFileReader = new BufferedReader(
				new FileReader(propertiesFile));
		while ((tempStr = propertiesFileReader.readLine()) != null) {
			matcher = pattern.matcher(tempStr);
			if (matcher.find()) {
				try {
					pageObject.callMethod(matcher.group(1), matcher.group(2),
							null, null, null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

}
