package com.auvn.browser.data;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import com.auvn.browser.data.pagefile.Page;
import com.auvn.browser.objects.MetaInfo;
import com.auvn.browser.objects.PageObject;
import com.auvn.browser.objects.interfaces.ClassesNames;
import com.auvn.browser.ui.popupmenus.SearchVariantsPopupMenu;

public class PagesData {
	private static final String PAGES_DIR = "./pages";

	private Map<String, PageObject> currentPage, tempPage;
	private ArrayList<Map<String, PageObject>> pages;
	private SearchVariantsPopupMenu popupMenu;

	private File pagesDirFile;

	public PagesData(SearchVariantsPopupMenu popupMenu) {
		this.popupMenu = popupMenu;
		pages = new ArrayList<Map<String, PageObject>>();
		createPagesDirFile();
	}

	private void createPagesDirFile() {
		pagesDirFile = new File(PAGES_DIR);
	}

	public void addPage(Map<String, PageObject> page) {
		// pages.add(currentPage = page);

	}

	public void loadMetas(Page page) {
		for (File tempFile : pagesDirFile.listFiles()) {
			if (tempFile.isFile() && tempFile.getName().contains(".ip"))
				try {
					tempPage = page.parsePage(tempFile);
					pages.add(tempPage);
					for (String key : tempPage.keySet())
						if (tempPage.get(key).getObjectType() == ClassesNames.META_INFO)
							popupMenu.addVariant((MetaInfo) tempPage.get(key));
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	public void clearPages() {
		pages.clear();
	}

	public Map<String, PageObject> getCurrentPage() {
		return currentPage;
	}

	public ArrayList<Map<String, PageObject>> getPages() {
		return pages;
	}

}
