package com.auvn.browser.data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import com.auvn.browser.objects.PageObject;
import com.auvn.browser.objects.Root;

public class SearchResultPageConstructor {
	public static final String RES_FILE_NAME = "./res_page.ip";

	private String variablesPart = "";
	private String initPart = "";
	private String location = "";
	private Map<String, PageObject> tempMap;
	private int linkIndex = 0;
	private FileWriter resultFileWriter;

	public SearchResultPageConstructor() {
		createResultFileWriter();
	}

	private void createResultFileWriter() {
		try {
			resultFileWriter = new FileWriter(new File(RES_FILE_NAME));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void createResultPageFile(ArrayList<Object[]> searchResultPages) {
		variablesPart = "";
		initPart = "";
		linkIndex = 0;
		System.out.println(searchResultPages);
		for (Object[] obj : searchResultPages) {
			tempMap = (Map<String, PageObject>) obj[0];
			location = ((Root) tempMap.get("root")).getRootLink();
			System.out.println(location + ":" + obj[1]);
			if (Integer.parseInt(obj[1].toString()) != 0) {
				addLinkVariable(location);
				addPageObject(location);
			}
		}
		writeToFile();
	}

	private void addLinkVariable(String location) {
		variablesPart += "link" + linkIndex + " = Link()\nlink" + linkIndex
				+ ".setLocation(\"" + location + "\")\n";
	}

	private void addPageObject(String link) {
		initPart += linkIndex + 1 + ". {link" + linkIndex + ":" + link
				+ "}\n\n";
		linkIndex++;
	}

	private void writeToFile() {
		try {
			createResultFileWriter();
			resultFileWriter.write(variablesPart);
			resultFileWriter.write("init:\n");
			resultFileWriter.write(initPart);
			resultFileWriter.flush();
			resultFileWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
