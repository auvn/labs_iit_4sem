package com.auvn.browser.data.pagefile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.auvn.browser.data.PagesData;
import com.auvn.browser.objects.AllPageObjects;
import com.auvn.browser.objects.PageObject;
import com.auvn.browser.objects.Root;
import com.auvn.browser.ui.MainFrame;

public class Page {
	private Map<String, PageObject> tempMap;
	private BufferedReader pageFileReader;
	private String tempStr;
	private Pattern newObjectPattern, invokeMethodPattern, initPattern;
	private Matcher objectsMatcher;
	private Pattern[] patterns;
	private MainFrame mainFrame;
	private PagesData pagesData;
	private String plainText, varName;
	private PageObject tempPageObject;
	private Root root;

	public Page(MainFrame mainFrame, PagesData pagesData) {
		this.mainFrame = mainFrame;
		this.pagesData = pagesData;
		createTempMap();
		createPatterns();
	}

	private void createPatterns() {
		newObjectPattern = Pattern.compile("^(.*)=(.*)[(](.*)[)]$");
		invokeMethodPattern = Pattern.compile("^(.*)[.](.*)[(](.*)[)]$");
		initPattern = Pattern.compile("^init:$");
		patterns = new Pattern[] { newObjectPattern, invokeMethodPattern,
				initPattern };
	}

	private void createTempMap() {
		tempMap = new HashMap<String, PageObject>();
	}

	private void createPageFileReader(File file) {
		try {
			pageFileReader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private int checkPattern(String match) {
		for (int i = 0; i < patterns.length; i++) {
			if ((objectsMatcher = patterns[i].matcher(match)).find())
				return i;
		}
		return -1;
	}

	private void newObject(String name, String type, String args)
			throws Exception {
		tempMap.put(name, (PageObject) AllPageObjects.getClass(type)
				.newInstance());
	}

	private void invokeMethod(String name, String method, String args)
			throws Exception {
		tempMap.get(name).callMethod(method, args, tempMap, pagesData, this);

	}

	private void createPlainText() {
		try {
			plainText = "";
			while ((tempStr = pageFileReader.readLine()) != null) {
				plainText += tempStr.trim() + "<br>";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private int getVarEnd(int i) {
		for (; i < plainText.length(); i++) {
			if (plainText.charAt(i) == ':')
				return i;
		}
		return -1;
	}

	private int startInit(PageObject pageObject, int curPos) {
		tempStr = "";
		String varValueText = "";

		for (int i = curPos, newPos = 0; i + 1 < plainText.length(); i++) {
			tempStr = plainText.substring(i, i + 1);
			if (tempStr.equals("{")) {
				varName = plainText.substring(i + 1, newPos = getVarEnd(i));
				i = startInit(tempMap.get(varName), newPos);
				varValueText += tempPageObject.getText();
			} else if (tempStr.equals("}") || i + 2 == plainText.length()) {
				pageObject.appendText(varValueText + tempStr);
				tempPageObject = pageObject;
				return i;
			} else {
				varValueText += tempStr;
			}
		}
		return -1;
	}

	private void init(File pageFile) {
		createPlainText();
		root = new Root();
		root.setRootLink(pageFile.getAbsoluteFile().toString());
		startInit(root, 0);
		tempMap.put("root", root);
	}

	public void show(Map<String, PageObject> pageMap) {
		mainFrame.getPagePanel().getPageEditorPane()
				.setText(pageMap.get("root").getText());
	}

	public Map<String, PageObject> parsePage(File pageFile) throws IOException {
		createTempMap();

		createPageFileReader(pageFile);
		while ((tempStr = pageFileReader.readLine()) != null) {
			try {
				switch (checkPattern(tempStr)) {
				case 0:
					newObject(objectsMatcher.group(1).trim(), objectsMatcher
							.group(2).trim(), objectsMatcher.group(3).trim());
					break;
				case 1:
					invokeMethod(objectsMatcher.group(1).trim(), objectsMatcher
							.group(2).trim(), objectsMatcher.group(3).trim());
					break;
				case 2:
					init(pageFile);
					break;
				case -1:
					System.out.println("");
					break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return tempMap;
	}
}
