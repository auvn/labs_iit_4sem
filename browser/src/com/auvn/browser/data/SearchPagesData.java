package com.auvn.browser.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import com.auvn.browser.objects.MetaInfo;
import com.auvn.browser.objects.PageObject;
import com.auvn.browser.objects.interfaces.ClassesNames;

public class SearchPagesData {
	private PagesData pagesData;
	private ArrayList<Object[]> pages;
	private Map<String, PageObject> currentPage;
	private PageObject tempPageObject;
	private String[] searchWords, currentMeta;
	private ArrayList<String> searchWordsList;
	private SearchResultPageConstructor searchResultPageConstructor;

	private String currentPageMetaText;
	private int count, tempInt = 0;

	public SearchPagesData(PagesData pagesData) {
		this.pagesData = pagesData;
		createPages();
		createSearchResultPageConstructor();
	}

	public void loadPages() {
		pages.clear();
		for (Map<String, PageObject> page : pagesData.getPages()) {
			pages.add(new Object[] { page, 0 });
		}
	}

	@SuppressWarnings("unchecked")
	public boolean search(String searchStr) {
		if (searchStr.length() != 0) {
			loadPages();
			currentPage = (Map<String, PageObject>) pages.get(0)[0];
			searchWords = searchStr.toLowerCase().split(" ");
			searchWordsList = new ArrayList<String>(Arrays.asList(searchWords));
			start();
			sortResults(pages);
			searchResultPageConstructor.createResultPageFile(pages);

			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private void start() {

		for (Object[] page : pages) {
			setDefaultPageParams();
			currentPage = (Map<String, PageObject>) page[0];
			for (String key : currentPage.keySet()) {
				if ((tempPageObject = currentPage.get(key)).getObjectType() == ClassesNames.META_INFO) {
					checkMeta((ArrayList<String>) searchWordsList.clone(),
							(MetaInfo) tempPageObject);
				}
			}
			if (currentPageMetaText.equals(""))
				currentPageMetaText = currentPage.get("root").getText()
						.toLowerCase();
			checkAllText(searchWordsList, currentPageMetaText);
			page[1] = count;
		}
	}

	private void checkMeta(ArrayList<String> list, MetaInfo metaInfo) {
		currentMeta = metaInfo.getMetaText().split(" ");

		tempInt = 0;

		for (String metaWord : currentMeta) {
			if (list.contains(metaWord)) {
				count += 3;
				tempInt++;
				list.remove(metaWord);
			}
		}
		if (tempInt == currentMeta.length) {
			count += tempInt * 4;
		}

		if (tempInt != 0 && list.size() != 0) {
			currentPageMetaText = metaInfo.getText().toLowerCase();
			for (String searchWord : list) {
				if (currentPageMetaText.contains(searchWord)) {
					count += 1;
				}
			}
		}

	}

	private void checkAllText(ArrayList<String> words, String text) {
		for (String word : words) {
			if (text.contains(word)) {
				count += 1;

			}
		}
	}

	private ArrayList<Object[]> sortResults(ArrayList<Object[]> pages) {
		Object[] iiObj, jjObj, tempObj;
		for (int i = 0, len = pages.size(), ii = 0, jj = 0; i < len; i++) {
			for (int j = 1; j < len - i; j++) {
				iiObj = pages.get(i);
				jjObj = pages.get(i + 1);
				ii = Integer.parseInt(iiObj[1].toString());
				jj = Integer.parseInt(jjObj[1].toString());
				System.out.println(ii + " " + jj);
				if (ii < jj) {
					tempObj = jjObj;
					pages.set(i + 1, iiObj);
					pages.set(i, tempObj);
				}
			}
		}
		return pages;
	}

	private void setDefaultPageParams() {
		count = 0;
		currentPageMetaText = "";
	}

	private void createPages() {
		pages = new ArrayList<Object[]>();
	}

	private void createSearchResultPageConstructor() {
		searchResultPageConstructor = new SearchResultPageConstructor();
	}

}
