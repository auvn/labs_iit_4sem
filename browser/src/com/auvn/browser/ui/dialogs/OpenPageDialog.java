package com.auvn.browser.ui.dialogs;

import java.io.File;

import javax.swing.JFileChooser;

import com.auvn.browser.ui.MainFrame;

public class OpenPageDialog {
	private JFileChooser openPageFileChooser;
	private int ret;
	private MainFrame mainFrame;

	public OpenPageDialog(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		createOpenPageFileChooser();
	}

	private void createOpenPageFileChooser() {
		openPageFileChooser = new JFileChooser();
		openPageFileChooser.setCurrentDirectory(new File("./"));
	}

	public File show() {
		ret = openPageFileChooser.showOpenDialog(mainFrame);
		if (ret == JFileChooser.APPROVE_OPTION) {
			return openPageFileChooser.getSelectedFile();
		}
		return null;
	}

}
