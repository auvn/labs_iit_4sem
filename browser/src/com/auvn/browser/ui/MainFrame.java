package com.auvn.browser.ui;

import javax.swing.JFrame;
import com.auvn.browser.ui.menubars.MainMenuBar;
import com.auvn.browser.ui.panels.PagePanel;

public class MainFrame extends JFrame {
	private PagePanel pagePanel;
	private MainMenuBar mainMenuBar;

	public MainFrame() {
		createPagePanel();
		createMainMenuBar();
		setFrameProperties();
	}

	private void createMainMenuBar() {
		mainMenuBar = new MainMenuBar();
		setJMenuBar(mainMenuBar);
	}

	private void createPagePanel() {
		if (pagePanel == null)
			pagePanel = new PagePanel();
		setContentPane(pagePanel);
	}

	private void setFrameProperties() {
		setSize(500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	public PagePanel getPagePanel() {
		return pagePanel;
	}

	public MainMenuBar getMainMenuBar() {
		return mainMenuBar;
	}

}
