package com.auvn.browser.ui.popupmenus;

import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;

import com.auvn.browser.objects.MetaInfo;
import com.auvn.browser.ui.MainFrame;
import com.auvn.browser.ui.actions.VariantsPopupMenuActions;

public class SearchVariantsPopupMenu extends JPopupMenu {
	private ArrayList<Object[]> variantsArrayList;

	private MainFrame mainFrame;
	private JTextField textField;
	private JMenuItem tempMenuItem;
	private boolean isAdded = false;

	private String variantStr;

	int x, y;

	public SearchVariantsPopupMenu(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
		this.textField = mainFrame.getPagePanel().getInstrumentsPanel()
				.getSearchTextField();
		this.x = textField.getX();
		this.y = (int) (textField.getY() + textField.getSize().getHeight());
		setAutoscrolls(true);
		createVariantsArrayList();

	}

	private void createVariantsArrayList() {
		variantsArrayList = new ArrayList<Object[]>();
	};

	public void show() {
		removeAll();
		isAdded = false;
		variantStr = textField.getText().toLowerCase();
		for (Object[] variant : variantsArrayList) {
			if (variant[0].toString().contains(variantStr)) {
				add((JMenuItem) variant[1]);
				isAdded = true;
			}

		}
		if (isAdded)
			super.show(textField, x, y);
	}

	public void addVariant(MetaInfo metaInfo) {
		tempMenuItem = new JMenuItem(variantStr = metaInfo.getMetaText());

		tempMenuItem.addMouseListener(new VariantsPopupMenuActions(mainFrame));

		variantsArrayList
				.add(new Object[] { variantStr, tempMenuItem });
	}
}
