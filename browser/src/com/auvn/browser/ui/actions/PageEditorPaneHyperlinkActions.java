package com.auvn.browser.ui.actions;

import java.io.File;
import java.io.IOException;

import javax.swing.JEditorPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import com.auvn.browser.data.PagesData;
import com.auvn.browser.data.pagefile.Page;

public class PageEditorPaneHyperlinkActions implements HyperlinkListener {
	private JEditorPane editorPane;
	private String urlStr;

	private Page page;
	private File pageFile;

	public PageEditorPaneHyperlinkActions(PagesData pagesData, Page page) {
		this.page = page;
	}

	public void hyperlinkUpdate(HyperlinkEvent he) {
		editorPane = (JEditorPane) he.getSource();
		urlStr = he.getDescription();

		if (he.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
			pageFile = new File(urlStr);
			try {
				page.show(page.parsePage(pageFile));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (he.getEventType() == HyperlinkEvent.EventType.ENTERED) {
			editorPane.setToolTipText(urlStr);
		} else if (he.getEventType() == HyperlinkEvent.EventType.EXITED) {
			editorPane.setToolTipText("");
		}
	}
}
