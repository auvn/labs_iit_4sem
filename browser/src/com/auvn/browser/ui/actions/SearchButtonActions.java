package com.auvn.browser.ui.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import com.auvn.browser.data.SearchPagesData;
import com.auvn.browser.data.SearchResultPageConstructor;
import com.auvn.browser.data.pagefile.Page;
import com.auvn.browser.ui.MainFrame;

public class SearchButtonActions implements ActionListener {
	private MainFrame mainFrame;
	private SearchPagesData searchPagesData;
	private Page page;

	public SearchButtonActions(MainFrame mainFrame,
			SearchPagesData searchPagesData, Page page) {
		this.mainFrame = mainFrame;
		this.searchPagesData = searchPagesData;
		this.page = page;
	}

	public void actionPerformed(ActionEvent arg0) {
		if (searchPagesData.search(mainFrame.getPagePanel()
				.getInstrumentsPanel().getSearchTextField().getText().trim()))
			try {
				page.show(page.parsePage(new File(
						SearchResultPageConstructor.RES_FILE_NAME)));
			} catch (IOException e) {
				e.printStackTrace();
			}

	}
}
