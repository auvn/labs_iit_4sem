package com.auvn.browser.ui.actions.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import com.auvn.browser.data.PagesData;
import com.auvn.browser.data.pagefile.Page;
import com.auvn.browser.ui.dialogs.OpenPageDialog;

public class OpenMenuItemAction implements ActionListener {
	private OpenPageDialog openPageDialog;
	private Page page;

	private File pageFile;

	public OpenMenuItemAction(OpenPageDialog openPageDialog, Page page,
			PagesData pagesData) {
		this.openPageDialog = openPageDialog;
		this.page = page;

	}

	public void actionPerformed(ActionEvent ae) {
		pageFile = openPageDialog.show();
		if (pageFile != null) {
			try {
				page.show(page.parsePage(pageFile));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("err");
		}
	}
}
