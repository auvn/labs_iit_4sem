package com.auvn.browser.ui.actions;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.auvn.browser.ui.MainFrame;
import com.auvn.browser.ui.popupmenus.SearchVariantsPopupMenu;

public class SearchTextFieldActions implements KeyListener {
	private SearchVariantsPopupMenu popupMenu;

	public SearchTextFieldActions(MainFrame mainFrame,
			SearchVariantsPopupMenu popupMenu) {
		this.popupMenu = popupMenu;
	}

	public void keyPressed(KeyEvent e) {

	}

	public void keyReleased(KeyEvent e) {
		popupMenu.show();

	}

	public void keyTyped(KeyEvent e) {
	};
}
