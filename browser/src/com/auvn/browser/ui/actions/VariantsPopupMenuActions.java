package com.auvn.browser.ui.actions;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JMenuItem;
import javax.swing.JTextField;

import com.auvn.browser.ui.MainFrame;

public class VariantsPopupMenuActions implements MouseListener {
	// private MainFrame mainFrame;
	private JMenuItem menuItem;
	private JTextField textField;

	public VariantsPopupMenuActions(MainFrame mainFrame) {
		textField = mainFrame.getPagePanel().getInstrumentsPanel()
				.getSearchTextField();
	}

	public void mouseClicked(MouseEvent e) {
		System.out.println(e.getSource());

	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {

	}

	public void mouseReleased(MouseEvent e) {
		menuItem = (JMenuItem) e.getSource();
		textField.setText(menuItem.getText());
		System.out.println("test");
	}

}
