package com.auvn.browser.ui.actions;

import javax.swing.JTextField;

import com.auvn.browser.data.PagesData;
import com.auvn.browser.data.SearchPagesData;
import com.auvn.browser.data.pagefile.Page;
import com.auvn.browser.ui.MainFrame;
import com.auvn.browser.ui.actions.menus.OpenMenuItemAction;
import com.auvn.browser.ui.dialogs.OpenPageDialog;
import com.auvn.browser.ui.menubars.menus.FileMenu;
import com.auvn.browser.ui.popupmenus.SearchVariantsPopupMenu;

public class AllActions {
	private OpenPageDialog openPageDialog;
	private MainFrame mainFrame;

	private Page page;
	private PagesData pagesData;
	private SearchPagesData searchPagesData;
	private SearchVariantsPopupMenu searchVariantsPopupMenu;

	private JTextField searchTextField;
	private FileMenu fileMenu;

	public AllActions() {
		createMainFrame();
		createOpenPageDialog();
		createSearchVariantsPopupMenu();
		createPagesData();
		createSearchPagesData();
		createPage();

		fileMenu = mainFrame.getMainMenuBar().getFileMenu();
		searchTextField = mainFrame.getPagePanel().getInstrumentsPanel()
				.getSearchTextField();

		fileMenu.getOpenMenuItem().addActionListener(
				new OpenMenuItemAction(openPageDialog, page, pagesData));

		searchTextField.addKeyListener(new SearchTextFieldActions(mainFrame,
				searchVariantsPopupMenu));

		mainFrame
				.getPagePanel()
				.getPageEditorPane()
				.addHyperlinkListener(
						new PageEditorPaneHyperlinkActions(pagesData, page));
		mainFrame
				.getPagePanel()
				.getInstrumentsPanel()
				.getSearchButton()
				.addActionListener(
						new SearchButtonActions(mainFrame, searchPagesData,
								page));

		pagesData.loadMetas(page);

	}

	private void createSearchVariantsPopupMenu() {
		searchVariantsPopupMenu = new SearchVariantsPopupMenu(mainFrame);
	}

	private void createMainFrame() {
		mainFrame = new MainFrame();
	}

	private void createOpenPageDialog() {
		openPageDialog = new OpenPageDialog(mainFrame);
	}

	private void createPagesData() {
		pagesData = new PagesData(searchVariantsPopupMenu);
	}

	private void createSearchPagesData() {
		searchPagesData = new SearchPagesData(pagesData);
	}

	private void createPage() {
		page = new Page(mainFrame, pagesData);
	}
}
