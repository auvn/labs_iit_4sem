package com.auvn.browser.ui.menubars;

import javax.swing.JMenuBar;

import com.auvn.browser.ui.menubars.menus.FileMenu;

public class MainMenuBar extends JMenuBar {
	private FileMenu fileMenu;

	public MainMenuBar() {
		createFileMenu();
		add(fileMenu);
	}

	private void createFileMenu() {
		fileMenu = new FileMenu();
	}

	public FileMenu getFileMenu() {
		return fileMenu;
	}

}
