package com.auvn.browser.ui.menubars.menus;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class FileMenu extends JMenu {
	private JMenuItem openMenuItem;

	public FileMenu() {
		setText("File");
		createOpenMenuItem();

		add(openMenuItem);
	}

	private void createOpenMenuItem() {
		openMenuItem = new JMenuItem("Open");
	}

	public JMenuItem getOpenMenuItem() {
		return openMenuItem;
	}

}
