package com.auvn.browser.ui.panels;

import java.awt.BorderLayout;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.auvn.browser.objects.PageObject;

public class PagePanel extends JPanel {
	private JEditorPane pageEditorPane;
	private InstrumentsPanel instrumentsPanel;

	public PagePanel() {
		createPageEditorPane();
		createInstrumentsPanel();

		setLayout(new BorderLayout());

		setPanelProperties();

		add(new JScrollPane(pageEditorPane), BorderLayout.CENTER);
		add(instrumentsPanel, BorderLayout.NORTH);
	}

	private void createInstrumentsPanel() {
		instrumentsPanel = new InstrumentsPanel();
	}

	private void createPageEditorPane() {
		pageEditorPane = new JEditorPane("text/html", "it works! browser ready");
		pageEditorPane.setEditable(false);

		pageEditorPane.setBorder(BorderFactory.createTitledBorder("Page"));

	}

	private void setPanelProperties() {
		// setBackground(Color.WHITE);
	}

	public JEditorPane getPageEditorPane() {
		return pageEditorPane;
	}

	public void setRoot(Map<String, PageObject> pageObjects, String root) {
		removeAll();
		add(pageObjects.get(root).getComponent(), BorderLayout.CENTER);
		updateUI();
	}

	public InstrumentsPanel getInstrumentsPanel() {
		return instrumentsPanel;
	}

}
