package com.auvn.browser.ui.panels;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import javax.swing.JTextField;

public class InstrumentsPanel extends JPanel {
	private JTextField searchTextField;
	private JButton searchButton;

	public InstrumentsPanel() {
		setLayout(new BorderLayout());
		createSearchTextField();
		createSearchButton();

		add(searchTextField, BorderLayout.CENTER);
		add(searchButton, BorderLayout.EAST);
	}

	private void createSearchButton() {
		searchButton = new JButton("Search");
	}

	private void createSearchTextField() {
		searchTextField = new JTextField();
	}

	public JTextField getSearchTextField() {
		return searchTextField;
	}

	public JButton getSearchButton() {
		return searchButton;
	}

}
