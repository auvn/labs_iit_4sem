package com.auvn.browser.objects;

public class Root extends PageObject {
	private String documentText = "";
	private String rootLink;

	public String getText() {
		return documentText;
	}

	public void setRootLink(String link) {
		rootLink = link;
	}

	public String getRootLink() {
		return rootLink;
	}

	public void appendText(String text) {
		documentText += text;
	}

}
