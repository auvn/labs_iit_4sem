package com.auvn.browser.objects;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.auvn.browser.data.propertiesfile.Properties;
import com.auvn.browser.objects.interfaces.ClassesNames;
import com.auvn.browser.objects.interfaces.MethodsNames;
import com.auvn.browser.objects.interfaces.PropertiesPatterns;

public class Text extends PageObject {
	private Map<String, Method> methods;
	private String text = "";
	private Object[] tempObjectsArray;
	private String textColor = "";
	private String textSize = "";
	private String textFont = "";

	public Text() {
		createMethods();
	}

	public void createMethods() {
		methods = new HashMap<String, Method>();
		try {
			methods.put(MethodsNames.SET_PROPERTIES_FILE, Text.class.getMethod(
					MethodsNames.SET_PROPERTIES_FILE, String.class));
			methods.put(MethodsNames.SET_TEXT,
					Text.class.getMethod(MethodsNames.SET_TEXT, String.class));
			methods.put(MethodsNames.SET_TEXT_COLOR, Text.class.getMethod(
					MethodsNames.SET_TEXT_COLOR, String.class));
			methods.put(MethodsNames.SET_BACKGROUND_COLOR, Text.class
					.getMethod(MethodsNames.SET_BACKGROUND_COLOR, String.class));
			methods.put(MethodsNames.SET_FONT,
					Text.class.getMethod(MethodsNames.SET_FONT, String.class));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void clear() {
		this.text = "";
	}

	public void appendText(String text) {
		if (text.length() > 1)
			this.text += text.substring(1, text.length() - 1);
	}

	public void setText(String text) {
		if (text.length() > 1)
			this.text = text.substring(1, text.length() - 1);
	}

	public void setFont(String args) {
		tempObjectsArray = args.split("[,]");
		textFont = "face=\"" + tempObjectsArray[0].toString() + "\"";
		textSize = " size=\"" + tempObjectsArray[1].toString() + "\"";
	}

	public void setBackgroundColor(String args) {
	}

	public void setTextColor(String args) {
		textColor = " color=\"" + args.substring(1, args.length() - 1) + "\"";
	}

	public void setPropertiesFile(String fileName) throws IOException {
		Properties.setProperties(
				new File(fileName.substring(1, fileName.length() - 1)), this,
				PropertiesPatterns.TEXT);
	}

	public void callMethod(String methodName, Object args, Object objectsMap,
			Object currentPagesData, Object pageParserClass)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {
		methods.get(methodName).invoke(this, args);
	}

	public String getText() {
		return "<font " + textFont + textColor + textSize + ">" + text
				+ "</font>";
	}

	public Map<String, Method> getMethods() {
		return methods;
	}

	public String getObjectType() {
		return ClassesNames.TEXT;
	}

}
