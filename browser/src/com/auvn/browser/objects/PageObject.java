package com.auvn.browser.objects;

import java.awt.Component;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import com.auvn.browser.objects.interfaces.PageObjectInterface;

public class PageObject implements PageObjectInterface {
	public void createMethods() {
	}

	public String getText() {
		return null;
	}

	public void appendText(String text) {
	}

	public void clear() {
	}

	public void setText(String text) {
	};

	public void callMethod(String methodName, Object args, Object objectsMap,
			Object currentPagesData, Object pageParserClass)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {

	}

	public Component getComponent() {
		return null;
	}

	public Map<String, Method> getMethods() {
		return null;
	}

	public String getObjectType() {
		return null;
	}

}
