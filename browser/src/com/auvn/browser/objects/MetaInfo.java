package com.auvn.browser.objects;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.auvn.browser.objects.interfaces.ClassesNames;
import com.auvn.browser.objects.interfaces.MethodsNames;

public class MetaInfo extends PageObject {
	private Map<String, Method> methods;
	private String metaText;
	private String text = "";

	public MetaInfo() {
		createMethods();
	}

	public void createMethods() {
		methods = new HashMap<String, Method>();
		try {
			methods.put(MethodsNames.SET_META_TEXT, MetaInfo.class.getMethod(
					MethodsNames.SET_META_TEXT, String.class));
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

	}

	public void appendText(String text) {
		if (text.length() > 1)
			this.text += text.substring(1, text.length() - 1);
	}

	public void setMetaText(String metaText) {
		if (metaText.length() > 1)
			this.metaText = metaText.substring(1, metaText.length() - 1)
					.toLowerCase();
	}

	public void callMethod(String methodName, Object args, Object objectsMap,
			Object currentPagesData, Object pageParserClass)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {
		methods.get(methodName).invoke(this, args);
	}

	public String getMetaText() {
		return metaText;
	}

	public String getText() {
		return text;
	}

	public String getObjectType() {
		return ClassesNames.META_INFO;
	}

	public Map<String, Method> getMethods() {
		return methods;
	}
}
