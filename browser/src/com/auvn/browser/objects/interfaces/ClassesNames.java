package com.auvn.browser.objects.interfaces;

public interface ClassesNames {
	public static final String TEXT = "Text";
	public static final String LINK = "Link";
	public static final String IMAGE = "Image";
	public static final String META_INFO = "MetaInfo";
	public static final String OBJECTS_PANEL = "ObjectsPanel";

}
