package com.auvn.browser.objects.interfaces;

import java.awt.Component;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

public interface PageObjectInterface {

	public void createMethods();

	public void setText(String text);

	public void appendText(String text);

	public String getText();

	public void clear();

	public Component getComponent();

	public void callMethod(String methodName, Object args, Object objectsMap,
			Object currentPagesData, Object pageParserClass)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException;

	public Map<String, Method> getMethods();

	public String getObjectType();
}
