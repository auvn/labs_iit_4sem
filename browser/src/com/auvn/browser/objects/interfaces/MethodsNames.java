package com.auvn.browser.objects.interfaces;

public interface MethodsNames {
	public static final String SET_PROPERTIES_FILE = "setPropertiesFile";

	public static final String SET_TEXT = "setText";
	public static final String SET_META_TEXT = "setMetaText";
	public static final String SET_BACKGROUND_COLOR = "setBackgroundColor";
	public static final String SET_TEXT_COLOR = "setTextColor";
	public static final String SET_FONT = "setFont";
	public static final String SET_LAYOUT = "setLayout";
	public static final String SET_IMAGE_FILE = "setImageFile";
	public static final String SET_SIZE = "setSize";
	public static final String ADD_OBJECT = "addObject";
	public static final String SET_LOCATION = "setLocation";

}
