package com.auvn.browser.objects.interfaces;

import java.util.regex.Pattern;

public interface PropertiesPatterns {
	public static final Pattern TEXT = Pattern.compile("^" + ClassesNames.TEXT
			+ "." + "(.*)[(](.*)[)]$");
	public static final Pattern LINK = Pattern.compile("^" + ClassesNames.LINK
			+ "." + "(.*)[(](.*)[)]$");
}
