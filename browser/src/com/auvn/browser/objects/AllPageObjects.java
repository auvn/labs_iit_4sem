package com.auvn.browser.objects;

import java.util.HashMap;
import java.util.Map;

import com.auvn.browser.objects.interfaces.ClassesNames;

public class AllPageObjects {
	public static Map<String, Class<?>> allObjects;

	static {
		allObjects = new HashMap<String, Class<?>>();
		allObjects.put(ClassesNames.TEXT, Text.class);
		allObjects.put(ClassesNames.LINK, Link.class);
		allObjects.put(ClassesNames.IMAGE, Image.class);
		allObjects.put(ClassesNames.META_INFO, MetaInfo.class);

	}

	public static Class<?> getClass(String className) {
		return allObjects.get(className);
	}
}
