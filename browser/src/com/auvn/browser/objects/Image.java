package com.auvn.browser.objects;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.auvn.browser.objects.interfaces.ClassesNames;
import com.auvn.browser.objects.interfaces.MethodsNames;

public class Image extends PageObject {

	private Map<String, Method> methods;
	private String[] tempStrs;
	private String startImageTag, endImageTag, widthValue, heightValue;

	public Image() {
		createMethods();
	}

	public void createMethods() {
		methods = new HashMap<String, Method>();
		try {
			methods.put(MethodsNames.SET_IMAGE_FILE, Image.class.getMethod(
					MethodsNames.SET_IMAGE_FILE, String.class));
			methods.put(MethodsNames.SET_SIZE,
					Image.class.getMethod(MethodsNames.SET_SIZE, String.class));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setImageFile(String imageFileName) {
		startImageTag = "<img src=" + imageFileName;
		endImageTag = "</img>";
	}

	public void setSize(String size) {
		tempStrs = size.split("[,]");
		widthValue = " width=" + tempStrs[0];
		heightValue = " height=" + tempStrs[1];
	}

	public void callMethod(String methodName, Object args, Object objectsMap,
			Object currentPagesData, Object pageParserClass)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {
		methods.get(methodName).invoke(this, args);
	}

	public Map<String, Method> getMethods() {
		return methods;
	}

	@Override
	public String getText() {
		return startImageTag + widthValue + heightValue + ">" + endImageTag;
	}

	public String getObjectType() {
		return ClassesNames.IMAGE;
	}

}
