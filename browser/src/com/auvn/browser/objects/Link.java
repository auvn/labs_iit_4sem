package com.auvn.browser.objects;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import com.auvn.browser.data.propertiesfile.Properties;
import com.auvn.browser.objects.interfaces.ClassesNames;
import com.auvn.browser.objects.interfaces.MethodsNames;
import com.auvn.browser.objects.interfaces.PropertiesPatterns;

public class Link extends Text {

	private String linkObjectUrl;
	private String startLinkTag, endLinkTag;

	public Link() {
		startLinkTag = "";
		endLinkTag = "";
		createMethods();
	}

	public void createMethods() {
		super.createMethods();
		try {
			super.getMethods().put(
					MethodsNames.SET_LOCATION,
					Link.class.getMethod(MethodsNames.SET_LOCATION,
							String.class));
			super.getMethods().put(
					MethodsNames.SET_PROPERTIES_FILE,
					Link.class.getMethod(MethodsNames.SET_PROPERTIES_FILE,
							String.class));
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}

	public void setLocation(String url) {
		linkObjectUrl = url.substring(1, url.length() - 1);
		startLinkTag = "<a href=\"" + linkObjectUrl + "\">";
		endLinkTag = "</a>";
	}

	public void setText(String text) {
		super.clear();
		super.appendText(text);
	}

	public void appendText(String text) {
		if (!(text.trim().length() == 0))
			setText(text);
	}

	public void setPropertiesFile(String fileName) throws IOException {	
		Properties.setProperties(
				new File(fileName.substring(1, fileName.length() - 1)), this,
				PropertiesPatterns.LINK);
	}

	public void callMethod(String methodName, Object args, Object objectsMap,
			Object currentPagesData, Object pageParserClass)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {
		super.callMethod(methodName, args, objectsMap, currentPagesData,
				pageParserClass);
	}

	public String getLocation() {
		return linkObjectUrl;
	}

	public String getObjectType() {
		return ClassesNames.LINK;
	}

	public String getText() {
		return startLinkTag + super.getText() + endLinkTag;
	}

}
