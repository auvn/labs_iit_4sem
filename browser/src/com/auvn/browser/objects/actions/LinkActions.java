package com.auvn.browser.objects.actions;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

import com.auvn.browser.data.PagesData;
import com.auvn.browser.data.pagefile.Page;
import com.auvn.browser.objects.Link;

public class LinkActions implements MouseListener {

	private Link link;
	private PagesData pagesData;
	private Page page;
	private File pageFile;

	public LinkActions(Link link) {
		this.link = link;
	}

	public void mouseClicked(MouseEvent e) {
		pageFile = new File(link.getLocation());
		try {
			pagesData.addPage(page.parsePage(pageFile));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {

	}

	public void mouseReleased(MouseEvent e) {

	}

	public void setPage(Page page) {
		this.page = page;
	}

	public void setPagesData(PagesData pagesData) {
		this.pagesData = pagesData;
	}

	public PagesData getPagesData() {
		return pagesData;
	}

}
